dirty_dataset<-read.csv("/Users/user/Downloads/dirty_data.csv")
dirty_dataset$Area<-gsub('\\s+','',dirty_dataset$Area)
dirty_dataset$Area[dirty_dataset$Area=='']<-NA
dirty_dataset$Area<-na.locf(dirty_dataset)

dirty_dataset$Street<-gsub("�[^[:alnum:][:blank:]+?&/\\-]",' ',dirty_dataset$Street)
dirty_dataset$Street<-gsub('�','',dirty_dataset$Street)
rem_padd<-function(x) gsub('^\\s+|\\s+$',"",x)
dirty_dataset$Street<-rem_padd(dirty_dataset$Street)
dirty_dataset$Street.2<-rem_padd(dirty_dataset$Street.2)
dat_ch<-dirty_dataset
dat_ch$Strange.HTML<-NULL
dat_ch$X.2<-NULL
dat_ch$X.1<-NULL
dat_ch$Street<-gsub('Street','Str.',dat_ch$Street)
dat_ch$Street<-gsub('Avenues','ave.',dat_ch$Street)
dat_ch$Street.2<-replace(dat_ch$Street.2,which(dat_ch$Street==dat_ch$Street.2),'')
write.csv(dat_ch,file ="/Users/user/Downloads/dirty_data.csv" )